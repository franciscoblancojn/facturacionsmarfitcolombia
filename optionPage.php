<?php
function FSGC_option_page_add_menu() {
	add_menu_page(
		'FSGC_option', // page <title>Title</title>
		'Facturacion SmarfitGo', // menu link text
		'manage_options', // capability to access the page
		'FSGC_option-slug', // page URL slug
		'FSGC_function_option_page', // callback function /w content
		'dashicons-grid-view', // menu icon
		5 // priority
	);
}
add_action( 'admin_menu', 'FSGC_option_page_add_menu' );
 
 
function FSGC_option_page_register_setting(){
    register_setting(
		PREFIX.'_option_settings', // settings group name
		PREFIX.'data', // option name
		'sanitize_text_field' // sanitization function
	);
 
	add_settings_section(
		PREFIX.'_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		PREFIX.'_option-slug' // page slug
	);
 
	add_settings_field(
		PREFIX.'data',
		PREFIX.'data',
		PREFIX.'_input', // function which prints the field
		PREFIX.'_option-slug', // page slug
		PREFIX.'_option_settings_section_id', // section ID
		array( 
			'class' => 'd-none', // for <tr> element
		)
    );
}
add_action( 'admin_init',  'FSGC_option_page_register_setting' );
function FSGC_input($atts)
{
    $text = get_option( PREFIX.'data' );
 
	printf(
		'<input type="text" id="'.PREFIX.'data'.'" name="'.PREFIX.'data'.'" value="%s" />',
		esc_attr( $text )
	);
}
function FSGC_function_option_page(){
    $FSGCdata = get_option( PREFIX.'data' );
    if($FSGCdata == "")
        $FSGCdata = "{}";
    $data = json_decode($FSGCdata,true);
    ?> 
    <div class="wrap">
	    <h1>Configuraciones de AWS</h1>
	    <form method="post" action="options.php">
            <table class="form-table" role="presentation">
                <tbody>
                    <?php
                        for ($i=0; $i < count(FSGC_Settings); $i++) { 
                            ?>
                            <tr>
                                <th scope="row">
                                    <label for="<?=FSGC_Settings[$i]?>">
                                    <?=FSGC_Settings[$i]?>
                                    </label>
                                </th>
                                <td data-children-count="1">
                                    <input 
                                    type="text" 
                                    id="<?=FSGC_Settings[$i]?>" 
                                    name="<?=FSGC_Settings[$i]?>" 
                                    value="<?=$data[FSGC_Settings[$i]]?>"
                                    onchange="saveData('<?=FSGC_Settings[$i]?>')"
                                    />
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                </tbody>
            </table>
            <?php
                settings_fields( PREFIX.'_option_settings' ); 
                do_settings_sections( PREFIX.'_option-slug' ); 
                submit_button();
            ?>
        </form>
        <style>
            .d-none{
                display:none;
            }
        </style>
        <script>
            var FSGCdata = <?=$FSGCdata?>;
            var inputFSGCdata = document.getElementById('FSGCdata')
            saveData = (id) => {
                e = document.getElementById(id)
                FSGCdata[id] = e.value
                
                console.log(FSGCdata);

                inputFSGCdata.value = JSON.stringify(FSGCdata)
            }
        </script>
    </div>
    <?php
}