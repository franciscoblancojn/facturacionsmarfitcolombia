<?php

class FSGC_api {
    private $URL = "http://161.35.131.92/";
    /**
     * __construct
     * encargado de inicializar FSGC_api
     */
    public function __construct($settings = array())
    {
        
    }
    /**
     * request
     *  encargado de hacer las peticiones a el API
     */
    public function request($json , $url , $method = "POST")
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "Content-Type=> application/json; charset=utf-8",
                "Accept=> application/json",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return ($response);
    }
    /**
     * getData
     * obtiene la data de option page
     */
    private function getData()
    {
        $FSGCdata = get_option( PREFIX.'data' );
        if($FSGCdata == "")
            $FSGCdata = "{}";
        return json_decode($FSGCdata,true);
    }
    /**
     * createFactura
     * crea el json necesario para la creacion de la factura
     */
    public function createFactura($order_id)
    {
        $order = wc_get_order( $order_id );

        $data = $this->getData();

        $items = array();
        foreach ( $order->get_items() as $item_id => $item ) {
            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            $product = wc_get_product( $product_id );
            $name = $item->get_name();
            $quantity = $item->get_quantity();
            $subtotal = $item->get_subtotal();
            $total = $item->get_total();
            $tax = $item->get_subtotal_tax();
            $taxclass = $item->get_tax_class();
            $taxstat = $item->get_tax_status();
            $allmeta = $item->get_meta_data();
            $somemeta = $item->get_meta( '_whatever', true );
            $type = $item->get_type();

            $items[] = array(
                "iva_base"          => 0.0, // Base de IVA detalle
                "description"       => $product->get_short_description(), // Description detalle
                "iva_value"         => 0.0, //Valor IVA
                "iva_percent"       => 0.0, //  Porcentaje de IVA
                "quantity"          => $quantity, // Cantidad
                "amount_before_tax" => $subtotal, // Valor antes de IVA
                "total_amount"      => $total // Total línea de detalle
            );
        }

        $json = array(
            "person_name"           => $data['person_name'], // Nombre del afiliado
            "document"              => $data['document'], // Numero de documento de identidad del afiliado
            "document_kind"         => $data['document_kind'], // Tipo de Documento de identidad=> CC, CE, TI
            "email"                 => $data['email'], // Email del afiliado
            "person_phone"          => array(
                "area_code"     => $data['person_phone_area_code'],
                "country_code"  => $data['person_phone_country_code'],
                "number"        => $data['person_phone_number']
            ),
            "person_address"        => $data['person_address'], // Direccion del Afiliado
            "city"                  => $data['city'], // Ciudad de la dirección del afiliado
            "date"                  => $order->get_date_created(), // Fecha de emisión de la factura
            "sequential"            => $data['sequential'],
            "iva_total"             => $order->get_total_tax(), // Total IVA en la factura
            "amount_before_tax"     => $order->get_total() - $order->get_total_tax(), // Valor total antes de IVA
            "total_amount"          => $order->get_total(), // Total incluido IVA
            "location_name"         => $data['location_name'], // Nombre de la sede donde está afiliado
            "location_address"      => $data['location_address'], // Dirección de la sede donde está afiliado
            "location_city"         => $data['location_city'], // Direccion de la sede
            "location_phone"        => array( // Telefono de la sede
                "area_code"     => $data['location_phone_area_code'],
                "country_code"  => $data['location_phone_country_code'],
                "number"        => $data['location_phone_number']
            ),
            "iva_base"              => $data['iva_base'], // Valor base para el calculo del IVA
            "invoice_uuid"          => $data['invoice_uuid'], // identificador de la factura en el sistema de origen
            "sequential_key"        => "", // NO SE USA Clave técnica de la resolucion
            "sequential_date_start" => "2019-09-01 00=>00=>00 UTC", // NO SE USA Fecha de la resolución de facturacion
            "sequential_start"      => 2577952, // NO SE USA Numero inicial de la autorización de facturacion
            "sequential_end"        => 3077951, // NO SE USA Numero final de la autorización de facturacion
            "dian_resolution"       => "18763000279766", // NO SE USA Numero de la autorización de facturacion
            // Lineas de Detalle de la factura
            "items"                 => $items,
        );
        $json = json_encode($json);
        update_post_meta($order_id,"JsonSend",$json);

        return $this->request($json,$this->URL."/app");
    }
}