<?php
function FSGC_add_function_order_change($order_id) {
    update_post_meta($order_id,"Send Order",true);

    $api = new FSGC_api();

    $r = $api->createFactura($order_id);

    update_post_meta($order_id, "respondeApi_createFactura",$r);
}
add_action('woocommerce_order_status_processing',   'FSGC_add_function_order_change' , 10, 1);  