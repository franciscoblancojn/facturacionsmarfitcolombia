<?php
/*
Plugin Name: Facturacion Smarfit
Plugin URI: https://startscoinc.com/es/
Description: Genera Facutura por pedido
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/*
License...
Copyright 2020 Startsco, Inc.
*/
define("PREFIX","FSGC");
define("FSGC_Settings", array(
    "person_name",
    "document",
    "document_kind",
    "email",
    "person_phone_area_code",
    "person_phone_country_code",
    "person_phone_number",
    "person_address",
    "city",
    "sequential",
    "location_name",
    "location_address",
    "location_city",
    "location_phone_area_code",
    "location_phone_country_code",
    "location_phone_number",
    "iva_base",
    "invoice_uuid",
));

require_once plugin_dir_path( __FILE__ )."api.php";
require_once plugin_dir_path( __FILE__ )."hook.php";
require_once plugin_dir_path( __FILE__ )."optionPage.php";